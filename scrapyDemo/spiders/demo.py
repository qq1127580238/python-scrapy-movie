# -*- coding: utf-8 -*-
import scrapy
import re
from ..items import ScrapydemoItem


class DemoSpider(scrapy.Spider):
    name = 'demo'
    allowed_domains = ['movie.douban.com']
    start_urls = ['http://movie.douban.com/top250']

    def parse(self, response):
        # html = response.text
        # reg = r'<img data-original="(.*?)">.*?<div class="li_txt">.*?<h3>(.*?)</h3>.*?<h4>(.*?)</h4>.*?<p>(.*?)</p>'
        # 获取每一个info的对象
        infos = response.xpath('//div[@class="item"]')
        for info in infos:
            # print(info.xpath('//div[@class="bd"]/p/text()'))
            item = ScrapydemoItem()
            item['url'] = info.css('a::attr(href)').extract_first()
            item['img'] = info.css('img::attr(src)').extract_first()
            title = info.xpath('//span[@class="title"]/text()').extract()
            item['title'] = title[0]
            item['english_title'] = title[1]
            item['other'] = info.xpath('//span[@class="other"]/text()').extract_first()
            movie_info = info.xpath('//div[@class="bd"]').re('<p class>(.*)<br>(.*)</p>')
            item['info'] = movie_info[0].strip()
            item['area'] = movie_info[1].strip()
            item['rating_num'] = info.xpath('//span[@class="rating_num"]/text()').extract_first()
            item['comment'] = info.xpath('//span/text()').re_first('(.*)人评价')
            # item['comment'] = info.re_first('<span>(.*?)人评价</span>')
            item['inq'] = info.xpath('//span[@class="inq"]/text()').extract_first()
            # 这里是用的yield 而不是return
            yield item
