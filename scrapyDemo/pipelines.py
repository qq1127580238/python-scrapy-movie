# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import json
from scrapy.pipelines.images import ImagesPipeline
from scrapy import Request


class ScrapydemoPipeline(object):

    # 初始化的操作，这里我们做本地化直接写成文件，所以初始化文件对象
    def __init__(self):
        print('实例化DemoPipeline')
        # self.f = open('movie_pipeline.json', 'w', encoding='utf-8')
        self.f = open('movie_pipeline.txt', 'w', encoding='utf-8')

    def process_item(self, item, spider):
        # content = json.dumps(dict(item))
        # self.f.write(content + ",")
        content = ''
        content += '[地址]：' + item['url']
        content += '\n'
        content += '[海报]：' + item['img']
        content += '\n'
        content += '[标题]：' + item['title']
        content += '\n'
        content += '[英文标题]：' + item['english_title']
        content += '\n'
        content += '[其他]：' + item['other']
        content += '\n'
        content += '[详情]：' + item['info']
        content += '\n'
        content += '[地区]：' + item['area']
        content += '\n'
        content += '[评分]：' + item['rating_num']
        content += '\n'
        content += '[评论人数]：' + item['comment']
        content += '\n'
        content += '[评语]：' + item['inq']
        content += '\n'
        content += '================'
        content += '\n'
        self.f.write(content + ",")
        # print(content)
        return item

    # def get_media_requests(self, item, info):
    #     yield Request(item['img'])

    # 结束后做的操作，在这里我们要关闭文件
    def close_spider(self, spider):
        print('结束')
        self.f.close()
