# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ScrapydemoItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    img = scrapy.Field()
    title = scrapy.Field()
    english_title = scrapy.Field()
    other = scrapy.Field()
    info = scrapy.Field()
    area = scrapy.Field()
    rating_num = scrapy.Field()
    comment = scrapy.Field()
    inq = scrapy.Field()
